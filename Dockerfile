FROM python:3.10
ADD ./requirements.txt /app/requirements.txt
RUN python -m pip install -r /app/requirements.txt
ADD . /app
WORKDIR /app
CMD [ "python", "__main__.py" ]
